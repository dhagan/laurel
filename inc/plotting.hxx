#include <iostream>
#include <TROOT.h>
#include <TH1.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TLatex.h>

#ifndef laurel_plot_hxx
#define laurel_plot_hxx

extern "C" struct box_edges{ float x1, y1, x2, y2; };

extern "C" typedef enum position{
  top_left, top_right, bottom_left, bottom_right, 
  below_logo, centre, top_right_right, top_right_left, 
  over_top_left, top_right_legend
} position;

namespace laurel {
  extern "C" void initialise();
}

namespace plotting {
  //namespace{
  //  bool recall;
  //}
  extern "C" box_edges positions( position pos = top_left );
  extern "C" TLegend * legend( position pos = top_left, int entries = 2 );
  extern "C" TPaveStats * stats( position pos = top_left, TH1 * hist=nullptr );
  extern "C" void axes_titles( TH1 * hist=nullptr, const std::string & x="", 
                              const std::string & y="" );
  extern "C" void title( const std::string & title="", position pos=over_top_left, bool sci_axis = false );
  extern "C" void atlas(  position pos = top_left, bool wip = false, bool simulation = false );
  extern "C" TH1F * ratio_subplot( TH1 * numerator, TH1 * denominator, TPad * pad=nullptr );
  extern "C" void write_histogram( TH1 * histogram, const std::string & name, const char * draw_option = "", const char * extras = "");



}

#endif

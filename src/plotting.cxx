#include <plotting.hxx>

#ifndef laurel_plot_cxx
#define laurel_plot_cxx

void laurel::initialise(){

  gROOT->SetStyle( "ATLAS" );
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();
  TGaxis::SetMaxDigits( 3 );
  
  uint8_t font=42;
  Double_t text_size = 0.034;
  gStyle->SetTextFont( font );
  gStyle->SetTextSize( text_size );
  for ( const char * axis : { "x", "y", "z" } ){
    gStyle->SetLabelSize( text_size, axis );
    gStyle->SetTitleSize( text_size, axis );
    gStyle->SetLabelFont( font, axis );
    gStyle->SetTitleFont( font, axis );

  }

  gStyle->SetLineStyleString( 2, "[12 12]" ); 
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  gStyle->SetPadTopMargin( 0.12 );
  gStyle->SetPadRightMargin( 0.12 );
  gStyle->SetPadBottomMargin( 0.12 );
  gStyle->SetPadLeftMargin( 0.12 );


  gROOT->ForceStyle();

}

box_edges plotting::positions( position pos ){ 

  switch ( pos ){
    case top_left:
      return box_edges{ 0.15, 0.82, 0.425, 0.85 };
    case top_right:
      return box_edges{ 0.65, 0.82, 0.85, 0.7 };
    case top_right_legend:
      return box_edges{ 0.6, 0.86, 0.85, 0.7 };
    case below_logo:
      return box_edges{ 0.187, 0.6, 0.4, 0.68 };
    case bottom_left:
      return box_edges{ 0.187, 0.187, 0.4, 0.4 };
    case bottom_right:
      return box_edges{ 0.7, 0.187, 0.88, 0.4 };
    case centre:
      return box_edges{ 0.4, 0.6, 0.4, 0.6 };
    case top_right_left:
      return box_edges{ 0.6, 0.6, 0.73, 0.78 };
    case top_right_right:
      return box_edges{ 0.74, 0.6, 0.88, 0.78 }; 
    case over_top_left:
      return box_edges{ 0.15, 0.89, 0.3, 1.2 }; 

  }
  return box_edges{ 0, 0, 0, 0 };
}

TLegend * plotting::legend( position pos, int entries ){

  box_edges edges = positions( pos );
  TLegend * legend = new TLegend( edges.x1, edges.y1, edges.x2, edges.y1 - 0.05*entries );
  legend->SetBorderSize( 0 );
  legend->SetFillColor( 0 );
  legend->SetFillStyle( 0 );
  legend->SetTextFont( 42 );
  legend->SetTextSize( 0.025 );
  //if ( hist ){ legend->AddEntry( hist, hist->GetName() ); }
  return legend;
}

TPaveStats * plotting::stats( position pos, TH1 * hist ){

  gPad->Update();
  TPaveStats * stats = (TPaveStats*) hist->FindObject( "stats" );
  box_edges edges = positions( pos );
  if ( stats ){
    stats->SetBorderSize( 1 );
    stats->SetFillStyle( 0 ); 
    stats->SetTextFont( 42 );
    stats->SetBorderSize( 1 );
    stats->SetX1NDC( edges.x1 ); 
    stats->SetX2NDC( edges.x2 );
    stats->SetY1NDC( edges.y1 ); 
    stats->SetY2NDC( edges.y2 );
    stats->Draw();
  } else {
      std::cout << stats << std::endl;
      std::cout << "Stats for " << hist->GetName() << " is nullptr." << std::endl;
      std::cout << "Enabling stats 'rmen' and retrying...." << std::endl;
      gStyle->SetOptStat( "rmen" ); 
      //recall = true;
      stats = plotting::stats( pos, hist );
    //}
  }
  return stats;
}

void plotting::axes_titles( TH1 * hist, const std::string & x, const std::string & y ){
  if ( !x.empty() ){ hist->GetXaxis()->SetTitle( x.c_str() ); }
  if ( !y.empty() ){ hist->GetYaxis()->SetTitle( y.c_str() ); }
}

void plotting::title( const std::string & title, position pos, bool sci_axis ){
  TList * prims = gPad->GetListOfPrimitives();
  TH1 * hist = nullptr;
  for ( TObject * object : *prims ){
    if ( object->InheritsFrom("TH1") ){ hist = (TH1 *) object; }
  }
  if ( hist ){
	  TLatex pad_title;
    pad_title.SetTextSize( 0.03 );
    pad_title.SetTextFont( 42 );
    box_edges edges = positions( pos );
    pad_title.DrawLatexNDC( (sci_axis) ? edges.x1 + 0.04 : edges.x1, edges.y1, title.c_str() );  
  }
}

void plotting::atlas( position pos, bool wip, bool sim ){

  box_edges edges = positions( pos );
  float & x1 = edges.x1;
  float & y1 = edges.y1;

  gPad->cd();
  TLatex * atlas_logo_ltx = new TLatex(); 
  atlas_logo_ltx->SetNDC(); 
  atlas_logo_ltx->SetTextSize(0.038);
  atlas_logo_ltx->SetTextFont(72);
  atlas_logo_ltx->SetTextColor(1);
  TLatex * wip_ltx = new TLatex(); 
  wip_ltx->SetNDC();
  wip_ltx->SetTextFont(42);
  wip_ltx->SetTextSize(0.038);
  wip_ltx->SetTextColor(1);
  TLatex * sim_ltx = new TLatex();
  sim_ltx->SetNDC();
  sim_ltx->SetTextFont(42);
  sim_ltx->SetTextSize(0.038);
  sim_ltx->SetTextColor(1);

  atlas_logo_ltx->DrawLatexNDC( x1, y1, "ATLAS" );
  if ( sim ){ sim_ltx->DrawLatexNDC( x1+0.13, y1, "Simulation" ); }
	if ( wip ){ wip_ltx->DrawLatexNDC( x1, y1-0.045, "Work In Progress" ); }
}

void plotting::write_histogram( TH1 * histogram, const std::string & name, 
                        const char * draw_option, const char * extras ){
  
  std::cout << extras << std::endl;
  TCanvas * canv = new TCanvas( "canv", "canv", 200, 200, 1000, 1000 );
  canv->Divide( 1 );
  canv->cd( 1 );
  histogram->Draw( draw_option ); 
  canv->SaveAs( name.c_str() );


}



TH1F * plotting::ratio_subplot( TH1 * numerator, TH1 * denominator, TPad * pad ){

  if ( pad ){
    pad->Divide( 1, 2, 0.0, 0.0 );
  } else {
    pad = (TPad *) gPad;
    pad->Divide( 1, 2, 0.0, 0.0 );
  }
  // prep ratio
  TH1F * ratio = (TH1F *) numerator->Clone();
  ratio->Reset();
  ratio->Divide( numerator, denominator, 1.0, 1.0 );
  ratio->SetLineColorAlpha( 1.0, 1.0 );
  ratio->SetLineStyle( 1.0 );
  ratio->SetLineWidth( 1.0 );

  // prep pads
  TPad * upper_subpad = (TPad *) pad->cd( 1 );
  upper_subpad->SetMargin( 0.12, 0.12, 0, 0.2);
  upper_subpad->SetPad( 0, 0.35, 1, 0.94);

  TPad * lower_subpad = (TPad *) pad->cd( 2 );
  lower_subpad->SetMargin( 0.12, 0.12, 0.25, 0 );
  lower_subpad->SetPad( 0, 0.06, 1.0, 0.35 );


  // upper pad
  upper_subpad = (TPad *) pad->cd( 1 );
  numerator->Draw( "E1" );
  denominator->Draw( "E1 SAMES" );

  // lower pad
  lower_subpad = (TPad *) pad->cd( 2 );
  lower_subpad->SetGridy();
  ratio->Draw( "E1" );
  ratio->SetStats( 0 );

  return ratio;

}



#endif
